"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var WowComponent = (function () {
    function WowComponent() {
        this.text = '<3 Angular2';
        this.test = false;
        this.clickMessage = '';
        this.clicks = 1;
    }
    WowComponent.prototype.deletes = function (event) {
        var test = false;
    };
    WowComponent.prototype.onClickMe2 = function (event) {
        var evtMsg = event ? '' : '';
        this.clickMessage = ("\u0423\u0434\u0430\u0447\u0430 =" + this.clicks++ + ". " + evtMsg);
    };
    WowComponent = __decorate([
        core_1.Component({
            selector: 'wow',
            template: "\n    {{text}}\n  ",
        }), 
        __metadata('design:paramtypes', [])
    ], WowComponent);
    return WowComponent;
}());
exports.WowComponent = WowComponent;
//# sourceMappingURL=wow.component.js.map