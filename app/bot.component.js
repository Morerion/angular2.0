"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var BotComponent = (function () {
    function BotComponent() {
        this.test = false;
        this.clickMessage = '';
        this.clicks = 1;
    }
    BotComponent.prototype.deletes = function (event) {
        var test = false;
    };
    BotComponent.prototype.onClickMe2 = function (event) {
        var evtMsg = event ? '' : '';
        this.clickMessage = ("\u0423\u0434\u0430\u0447\u0430 =" + this.clicks++ + ". " + evtMsg);
    };
    BotComponent = __decorate([
        core_1.Component({
            selector: 'Bot',
            template: "\n  <br>\n  <div class='container'>\n    <button type=\"button\" class=\"btn btn-primary btn-lg btn-block\" (click)=\"onClickMe2($event)\">+1 \u043A \u0443\u0434\u0430\u0447\u0435</button>\n    <button type=\"button\" class=\"btn btn-secondary btn-lg btn-block\" (click)=\"deletes($event)\">\u0423\u0434\u0430\u043B\u0438\u0442\u044C \u0442\u0443\u0442 \u0432\u0441\u0435 ******</button>\n    {{clickMessage}}\n  </div>\n  ",
        }), 
        __metadata('design:paramtypes', [])
    ], BotComponent);
    return BotComponent;
}());
exports.BotComponent = BotComponent;
//# sourceMappingURL=bot.component.js.map