import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
  <div *ngIf='money'>
  <div *ngIf='doom'>
  <nav class="navbar navbar-light bg-faded">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar" aria-controls="exCollapsingNavbar" aria-expanded="false" aria-label="Toggle navigation">
      &#9776;
    </button>
    <div class="collapse" id="exCollapsingNavbar">
      <div class="bg-light p-a-1">
        <h4>Angularchik 2.0 for Artem</h4>
        <span class="text-muted">Тут используются и ng и не ng, есть ngFor, ngIf, (click), я бы мог сделать подгрузку с Firebase, но мне кажется что стоит изучить AngularFire2, так же можно разбить все части на отдельные компоненты, но это тоже будет чуть позже, (Нужны (Вроде бы) Derectives/Provides для коректной работы с ngIf) Внизу приведен пример </span>
      </div>
    </div>
  </nav>
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
  <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  <li data-target="#carousel-example-generic" data-slide-to="2"></li>
</ol>
<div class="carousel-inner" role="listbox">
  <div class="carousel-item active">
    <img src="http://dreempics.com/img/picture/Jul/13/8a71b2b109b18abdf4a2a2a8e0007bdc/2.jpg" alt="First slide">
  </div>
  <div class="carousel-item">
    <img src="http://dreempics.com/img/picture/Jul/13/8a71b2b109b18abdf4a2a2a8e0007bdc/2.jpg" alt="Second slide">
  </div>
  <div class="carousel-item">
    <img src="http://dreempics.com/img/picture/Jul/13/8a71b2b109b18abdf4a2a2a8e0007bdc/2.jpg" alt="Third slide">
  </div>
</div>
<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
  <span class="icon-prev" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
  <span class="icon-next" aria-hidden="true"></span>
  <span class="sr-only">Next</span>
</a>
</div>
<br>
<br>
<br>
<br>
<div class="container marketing">

  <!-- Three columns of text below the carousel -->
  <div class="row">
    <div class="col-lg-4 text-xs-center">
      <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
      <h2>Heading</h2>
      <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
      <p><a class="btn btn-secondary" role="button" (click)="buy(money)">View details &raquo;</a></p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4 text-xs-center">
      <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
      <h2>Heading</h2>
      <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
      <p><a class="btn btn-secondary" role="button" (click)="buy(money)">View details &raquo;</a></p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4 text-xs-center">
      <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" width="140" height="140">
      <h2>Heading</h2>
      <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
      <p><a class="btn btn-secondary" role="button" (click)="buy(money)">View details &raquo;</a></p>
    </div><!-- /.col-lg-4 -->
  </div><!-- /.row -->
  <br>
  <div class='container'>
    <button type="button" class="btn btn-primary btn-lg btn-block" (click)="onClickMe2($event)">+1 к удаче</button>
    <p>{{clickMessage}}</p>
    <button type="button" class="btn btn-secondary btn-lg btn-block" (click)="deletes(doom)">Удалить тут все ******</button>
    <br>
    <p>Следующий текст подгружен из отдельного Component:
    <wow>Что-то пошло не так((</wow>
      <input (keyup)="onKey($event)">
      <p>{{values}}</p>
  </div>
  </div>
  </div>
  </div>
  <div *ngIf='!doom'>
   <h1>{{block}}</h1>
  </div>
  <div *ngIf='!money'>
   <h1>{{mone}}</h1>
  </div>

  `,
  styleUrls: ['app/dont-touch.component.css']
})
export class AppComponent {
  values = '';

/*
// without strong typing
onKey(event:any) {
  this.values += event.target.value + ' | ';
}
*/
// with strong typing
onKey(event: KeyboardEvent) {
  this.values += (<HTMLInputElement>event.target).value + ' | ';
}
  money = true;
  block = 'Вот и все.'
  doom = true;
  mone = 'К сожалению, купить сейчас что либо невозможно...';
  google = 'Ты сам захотел';
  clickMessage = '';
  clicks = 1;
  deletes(doom) {
    alert('doom = false');
    console.log(doom);
    this.doom = !doom;
  }
  buy(money) {
    this.money = !money;
  }
  onClickMe2(event: any) {
    let evtMsg = event ? ''  : '';
    this.clickMessage = (`Удача: #${this.clicks++}. ${evtMsg}`);
  }
}


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
