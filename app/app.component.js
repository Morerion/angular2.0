"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var AppComponent = (function () {
    function AppComponent() {
        this.values = '';
        this.money = true;
        this.block = 'Вот и все.';
        this.doom = true;
        this.mone = 'К сожалению, купить сейчас что либо невозможно...';
        this.google = 'Ты сам захотел';
        this.clickMessage = '';
        this.clicks = 1;
    }
    /*
    // without strong typing
    onKey(event:any) {
      this.values += event.target.value + ' | ';
    }
    */
    // with strong typing
    AppComponent.prototype.onKey = function (event) {
        this.values += event.target.value + ' | ';
    };
    AppComponent.prototype.deletes = function (doom) {
        alert('doom = false');
        console.log(doom);
        this.doom = !doom;
    };
    AppComponent.prototype.buy = function (money) {
        this.money = !money;
    };
    AppComponent.prototype.onClickMe2 = function (event) {
        var evtMsg = event ? '' : '';
        this.clickMessage = ("\u0423\u0434\u0430\u0447\u0430: #" + this.clicks++ + ". " + evtMsg);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n  <div *ngIf='money'>\n  <div *ngIf='doom'>\n  <nav class=\"navbar navbar-light bg-faded\">\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#exCollapsingNavbar\" aria-controls=\"exCollapsingNavbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n      &#9776;\n    </button>\n    <div class=\"collapse\" id=\"exCollapsingNavbar\">\n      <div class=\"bg-light p-a-1\">\n        <h4>Angularchik 2.0 for Artem</h4>\n        <span class=\"text-muted\">\u0422\u0443\u0442 \u0438\u0441\u043F\u043E\u043B\u044C\u0437\u0443\u044E\u0442\u0441\u044F \u0438 ng \u0438 \u043D\u0435 ng, \u0435\u0441\u0442\u044C ngFor, ngIf, (click), \u044F \u0431\u044B \u043C\u043E\u0433 \u0441\u0434\u0435\u043B\u0430\u0442\u044C \u043F\u043E\u0434\u0433\u0440\u0443\u0437\u043A\u0443 \u0441 Firebase, \u043D\u043E \u043C\u043D\u0435 \u043A\u0430\u0436\u0435\u0442\u0441\u044F \u0447\u0442\u043E \u0441\u0442\u043E\u0438\u0442 \u0438\u0437\u0443\u0447\u0438\u0442\u044C AngularFire2, \u0442\u0430\u043A \u0436\u0435 \u043C\u043E\u0436\u043D\u043E \u0440\u0430\u0437\u0431\u0438\u0442\u044C \u0432\u0441\u0435 \u0447\u0430\u0441\u0442\u0438 \u043D\u0430 \u043E\u0442\u0434\u0435\u043B\u044C\u043D\u044B\u0435 \u043A\u043E\u043C\u043F\u043E\u043D\u0435\u043D\u0442\u044B, \u043D\u043E \u044D\u0442\u043E \u0442\u043E\u0436\u0435 \u0431\u0443\u0434\u0435\u0442 \u0447\u0443\u0442\u044C \u043F\u043E\u0437\u0436\u0435, (\u041D\u0443\u0436\u043D\u044B (\u0412\u0440\u043E\u0434\u0435 \u0431\u044B) Derectives/Provides \u0434\u043B\u044F \u043A\u043E\u0440\u0435\u043A\u0442\u043D\u043E\u0439 \u0440\u0430\u0431\u043E\u0442\u044B \u0441 ngIf) \u0412\u043D\u0438\u0437\u0443 \u043F\u0440\u0438\u0432\u0435\u0434\u0435\u043D \u043F\u0440\u0438\u043C\u0435\u0440 </span>\n      </div>\n    </div>\n  </nav>\n  <div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\">\n<ol class=\"carousel-indicators\">\n  <li data-target=\"#carousel-example-generic\" data-slide-to=\"0\" class=\"active\"></li>\n  <li data-target=\"#carousel-example-generic\" data-slide-to=\"1\"></li>\n  <li data-target=\"#carousel-example-generic\" data-slide-to=\"2\"></li>\n</ol>\n<div class=\"carousel-inner\" role=\"listbox\">\n  <div class=\"carousel-item active\">\n    <img src=\"http://dreempics.com/img/picture/Jul/13/8a71b2b109b18abdf4a2a2a8e0007bdc/2.jpg\" alt=\"First slide\">\n  </div>\n  <div class=\"carousel-item\">\n    <img src=\"http://dreempics.com/img/picture/Jul/13/8a71b2b109b18abdf4a2a2a8e0007bdc/2.jpg\" alt=\"Second slide\">\n  </div>\n  <div class=\"carousel-item\">\n    <img src=\"http://dreempics.com/img/picture/Jul/13/8a71b2b109b18abdf4a2a2a8e0007bdc/2.jpg\" alt=\"Third slide\">\n  </div>\n</div>\n<a class=\"left carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"prev\">\n  <span class=\"icon-prev\" aria-hidden=\"true\"></span>\n  <span class=\"sr-only\">Previous</span>\n</a>\n<a class=\"right carousel-control\" href=\"#carousel-example-generic\" role=\"button\" data-slide=\"next\">\n  <span class=\"icon-next\" aria-hidden=\"true\"></span>\n  <span class=\"sr-only\">Next</span>\n</a>\n</div>\n<br>\n<br>\n<br>\n<br>\n<div class=\"container marketing\">\n\n  <!-- Three columns of text below the carousel -->\n  <div class=\"row\">\n    <div class=\"col-lg-4 text-xs-center\">\n      <img class=\"img-circle\" src=\"data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==\" alt=\"Generic placeholder image\" width=\"140\" height=\"140\">\n      <h2>Heading</h2>\n      <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>\n      <p><a class=\"btn btn-secondary\" role=\"button\" (click)=\"buy(money)\">View details &raquo;</a></p>\n    </div><!-- /.col-lg-4 -->\n    <div class=\"col-lg-4 text-xs-center\">\n      <img class=\"img-circle\" src=\"data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==\" alt=\"Generic placeholder image\" width=\"140\" height=\"140\">\n      <h2>Heading</h2>\n      <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>\n      <p><a class=\"btn btn-secondary\" role=\"button\" (click)=\"buy(money)\">View details &raquo;</a></p>\n    </div><!-- /.col-lg-4 -->\n    <div class=\"col-lg-4 text-xs-center\">\n      <img class=\"img-circle\" src=\"data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==\" alt=\"Generic placeholder image\" width=\"140\" height=\"140\">\n      <h2>Heading</h2>\n      <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>\n      <p><a class=\"btn btn-secondary\" role=\"button\" (click)=\"buy(money)\">View details &raquo;</a></p>\n    </div><!-- /.col-lg-4 -->\n  </div><!-- /.row -->\n  <br>\n  <div class='container'>\n    <button type=\"button\" class=\"btn btn-primary btn-lg btn-block\" (click)=\"onClickMe2($event)\">+1 \u043A \u0443\u0434\u0430\u0447\u0435</button>\n    <p>{{clickMessage}}</p>\n    <button type=\"button\" class=\"btn btn-secondary btn-lg btn-block\" (click)=\"deletes(doom)\">\u0423\u0434\u0430\u043B\u0438\u0442\u044C \u0442\u0443\u0442 \u0432\u0441\u0435 ******</button>\n    <br>\n    <p>\u0421\u043B\u0435\u0434\u0443\u044E\u0449\u0438\u0439 \u0442\u0435\u043A\u0441\u0442 \u043F\u043E\u0434\u0433\u0440\u0443\u0436\u0435\u043D \u0438\u0437 \u043E\u0442\u0434\u0435\u043B\u044C\u043D\u043E\u0433\u043E Component:\n    <wow>\u0427\u0442\u043E-\u0442\u043E \u043F\u043E\u0448\u043B\u043E \u043D\u0435 \u0442\u0430\u043A((</wow>\n      <input (keyup)=\"onKey($event)\">\n      <p>{{values}}</p>\n  </div>\n  </div>\n  </div>\n  </div>\n  <div *ngIf='!doom'>\n   <h1>{{block}}</h1>\n  </div>\n  <div *ngIf='!money'>\n   <h1>{{mone}}</h1>\n  </div>\n\n  ",
            styleUrls: ['app/dont-touch.component.css']
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
//# sourceMappingURL=app.component.js.map